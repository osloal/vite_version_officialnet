module.exports = {
    plugins:[
        require('postcss-pxtorem')({
                rootValue: 100,
                propList: ['*'],
                selectorBlackList: [],
                minPixelValue: 2
        })
    ]
}