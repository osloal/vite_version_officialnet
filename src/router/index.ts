import { createRouter, createWebHashHistory ,createWebHistory} from "vue-router";
const routes= [
  {
    path: "/",
    name: "Main",
    component: () => import("../Views/Main/Main.vue"),
    meta: {
      index: 1,
      keepAlive: false,
    },
  },
  {
    path: "/login",
    name: "Login",
    component: () => import("../Views/Login/Login.vue"),
    meta: {
      index: 1,
      keepAlive: true,
    },
  },
  {
    path: "/mountain",
    name: "Mountain",
    component: () => import("../Views/Mountain/index.vue"),
    meta: {
      index: 1,
      keepAlive: true,
    }
  },
  {
    path: "/house",
    name: "house",
    component: () => import("../Views/House/index.vue"),
    meta: {
      index: 1,
      keepAlive: true,
    },
  },
  {
    path: "/about",
    name: "about",
    component: () => import("../Views/AboutUs/AboutUs.vue"),
    meta: {
      index: 1,
      keepAlive: true,
    },
  },
  {
    path:"/roam",
    name:"roam",
    component:()=>import("../Views/Roam/index.vue"),
    meta:{
      index:1,
      keepAlive:true,
    }
  },
  {
    path:'/codeEditor',
    name:'codeEditor',
    component:()=>import("@/Views/Tools/codeEditor.vue"),
    meta:{
      index:1,
      keepAlive:true,
    }
  },
  {
    path:'/configtest',
    name:'configtest',
    component:()=>import("@/Views/Test/index.vue"),
    meta:{
      index:1,
      keepAlive:true,
    }
  }
];
let router = createRouter({
  //createWebHistory
  // history: createWebHistory(),   //hash模式
  history: createWebHashHistory(),//history模式
  routes,
});
// 设置全局前置守卫
// router.beforeEach((to, from, next) => {
//   // 在这里可以添加你的路由守卫逻辑
//   next(); // 要确保调用 next()，否则路由可能无法正常切换
// });

// 设置全局后置钩子
// router.afterEach((to, from) => {
//     if(from.path =='/mountain'){
//     }
// });
export default router;
