import { defineStore } from "pinia";

export const pageStatusStore = defineStore("pageStatus", {
  state: () => {
    return {
      mainPageScrollHeight: 0,                                                          //记录主页面滚动的位置进行实时保存进行后续的更新
      modelLoadingStatus: true,                                                         //记录模型加载状态
      recordPlatformLanguage: initWebStatusConfig.getPlatformLanguage() || 'zh',        //记录当前平台的语言
    };
  },
  actions: {
    
  },
  getters: {
    
  },
  persist: true,
});
const initWebStatusConfig = {
  //获取当前运行平台的语言
  getPlatformLanguage: ()=>{
      const language = navigator.language;
      const arr = language.split('-');
      return arr[0];
  }
}