import { defineStore } from "pinia";

export const loginStatusStore = defineStore("loginStatus", {
  state: () => {
    return {
      isLogin:false,
      username:"",
      role:'customer'
    };
  },
  actions: {
    
  },
  getters: {
    
  },
  persist: true,
});