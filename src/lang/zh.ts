const messages = {
    home: {
      title: '书城',
      hint: '计算机科学和软件工程',
      guessYouLike: '猜你喜欢'
    },
    roam:{
      tips:'鼠标单击开始漫游',
      operation:'(W, A, S, D = 移动, SPACE(空格键) = 跳跃, R = 开关碰撞检测,  MOUSE(鼠标) = 控制方向)',
      mode:'漫游模式已打开,按R键切换'
    },
    register:{
      backtips:'返回登录',
      title:'注册',
      username:'用户名',
      password:'密码',
      repassword:'确认密码',
      telephone:'手机号码',
      register:'注册'
    },
    header:{
      firstLevel:{
        officeName:'这里是我的名字',
        home:'主页',
        solution:'解决方案',
        productCenter:'产品中心',
        serviceSupport:'服务支持',
        aboutUs:'关于我们',
        Login:'登录',
        outLogin:'退出登录'
      },
      secondLevel:{
        home:{},
        solution:{
          metaverse:'元宇宙',
          digitalCity:'数字城市',
          digitalTwin:'数字孪生',
          tdExhibition:'3D展览',
          digitalMuseum:'数字化博物馆',
          systemCustomDevelopment:'系统定制开发',
        },
        productCenter:{},
        serviceSupport:{
          systemDevelopment:'系统开发',
          threeJsSystemCustomization:'ThreeJs系统定制开发',
          technicalSupport:'技术支持',
        },
        aboutUs:{
          aboutSiRui:'关于思瑞',
          corporateCulture:'企业文化',
          newsEvents:'新闻事件',
          contactUs:'联系我们'
        }
      },
      
    },
    body:{
      bannerFirst:{
        header:'We offer modern solutions for growing your business',
        body:'We are team of talented designers making websites with Smart Digital City',
        start:'Get Started'
      },
      bannerSecond:{
        header:'What Can We Do',
        body:`ThreeJS is now a leading front-end 3d construction tool that can quickly implement 3D projects with good performance. 
          Our aim is to help users build and realize businesses in their respective fields.`,
        leg:`hreeJS现在是领先的前端3D构建工具，可以快速实现性能良好的3D项目。
          我们的目标是帮助用户在各自领域建立和实现业务。`,
        readMore:'Read More'
      },
      bannerInsert:{
        title:'Roam your operational inspirations',
        contentEN:`You can freely control the keyboard and mouse to move your perspective within existing scenes,  
          allowing you to have a 360-degree panoramic view of the internal environment and surrounding surroundings. 
          This aims to  further advance towards the path of metaverse-like experiences!`,
        contentZH:'你可以在已存在的场景中，通过控制键鼠去自由移动你的视角，360°全方位观看内部场景以及周围环境。旨在朝类元宇宙赛道的继续进军！',
      },
      bannerThird:{
        title1:'Our Value',
        title2:'Odit est perspiciatis laborum et dicta',
        body:{
          v1:{
            title:'Ad cupiditate sed est odio',
            content:'Eum ad dolor et. Autem aut fugiat debitis voluptatem consequuntur sit. Et veritatis id.',
          },
          v2:{
            title:'Voluptatem voluptatum alias',
            content:'Repudiandae amet nihil natus in distinctio suscipit id. Doloremque ducimus ea sit non.'
          },
          v3:{
            title:'Fugit cupiditate alias nobis.',
            content:'Quam rem vitae est autem molestias explicabo debitis sint. Vero aliquid quidem commodi.'
          }
        }
      },
      bannerFourth:{
        header:'联系我们',
        location:{
          title:'地址',
          tip1:'杭州',
          tip2:'上城'
        },
        phone:{
          title:'联系我们',
          tip1:'18856635670',
          tip2:'xxxxxxxx'
        },
        message:{
          title:'邮件',
          tip1:`468133459{'@'}qq.com`,
          tip2:''
        },
        timer:{
          title:'营业时间',
          tip1:'全天',
          tip2:'周一至周六'
        },
        sendCustomName:'输入您的姓名',
        sendCustomEmail:'输入您的邮箱',
        sendCustomTheme:'主题',
        sendCustomContent:'请输入信息',
        submit:'提交'
      }
    },
    footer:{
      Name:'思瑞芸',
      Intro:`Cras fermentum odio eu feugiat lide par naso tierra. Justo eget nada terra videa magna
          derita valies darta donna mare fermentum iaculis eu non diam phasellus.`,
      userfulLink:'友情链接',
      homepage:'主页',
      aboutus:'关于我们',
      service:'服务',
      privacy:'隐私政策',

      webSystemDevelopment:'网站系统定制开发',
      threeJsDevelopment:'ThreeJs系统定制开发',
      metacosmicDevelopment:'元宇宙开发',
      exhibition:'3D会展开发',
      digitalTwin:'数字孪生',

      contactUs:'联系我们',
      location:'上城区',
      city:'杭州',
      province:'浙江省',
      
      phone:'电话',
      email:'邮箱',
    }
  }
   
  export default messages