const messages = {
    home: {
      title: 'Book Store',
      hint: 'Computer Science And Software Engineering',
      guessYouLike: 'Guess You Like',
    },
    roam:{
      tips:'Mouse click to start roaming',
      operation:'(W, A, S, D = move, SPACE = jump, R = switch collision detection, MOUSE = control direction)',
      mode:'Roaming mode is on. Press R to switch'
    },
    register:{
      backtips:'Back To Login',
      title:'Sign In',
      username:'UserName',
      password:'PassWord',
      repassword:'ConfirmPassword',
      telephone:'PhoneNumber',
      register:'register'
    },
    header:{
      firstLevel:{
        officeName:'Here is my name',
        home:'Home',
        solution:'Solution',
        productCenter:'Product',
        serviceSupport:'Support',
        aboutUs:'aboutUs',
        Login:'Log in',
        outLogin:'Log out'
      },
      secondLevel:{
        home:{},
        solution:{
          metaverse:'Meta',
          digitalCity:'DigitalCity',
          digitalTwin:'DigitalTwin',
          tdExhibition:'3DExhibition',
          digitalMuseum:'DigitalMuseum',
          systemCustomDevelopment:'SystemDevelopment',
        },
        productCenter:{},
        serviceSupport:{
          systemDevelopment:'SystemDevelopment',
          threeJsSystemCustomization:'ThreeJsSystemCustomization',
          technicalSupport:'TechnicalSupport',
        },
        aboutUs:{
          aboutSiRui:'AboutSiRui',
          corporateCulture:'CorporateCulture',
          newsEvents:'NewsEvents',
          contactUs:'ContactUs'
        }
      },
      
    },
    body:{
      bannerFirst:{
        header:'We offer modern solutions for growing your business',
        body:'We are team of talented designers making websites with Smart Digital City',
        start:'Get Started'
      },
      bannerSecond:{
        header:'What Can We Do',
        body:`ThreeJS is now a leading front-end 3d construction tool that can quickly implement 3D projects with good performance. 
          Our aim is to help users build and realize businesses in their respective fields.`,
        leg:`hreeJS现在是领先的前端3D构建工具，可以快速实现性能良好的3D项目。
          我们的目标是帮助用户在各自领域建立和实现业务。`,
        readMore:'Read More'
      },
      bannerInsert:{
        title:'Roam your operational inspirations',
        contentEN:`You can freely control the keyboard and mouse to move your perspective within existing scenes,  
          allowing you to have a 360-degree panoramic view of the internal environment and surrounding surroundings. 
          This aims to  further advance towards the path of metaverse-like experiences!`,
        contentZH:'你可以在已存在的场景中，通过控制键鼠去自由移动你的视角，360°全方位观看内部场景以及周围环境。旨在朝类元宇宙赛道的继续进军！',
      },
      bannerThird:{
        title1:'Our Value',
        title2:'Odit est perspiciatis laborum et dicta',
        body:{
          v1:{
            title:'Ad cupiditate sed est odio',
            content:'Eum ad dolor et. Autem aut fugiat debitis voluptatem consequuntur sit. Et veritatis id.',
          },
          v2:{
            title:'Voluptatem voluptatum alias',
            content:'Repudiandae amet nihil natus in distinctio suscipit id. Doloremque ducimus ea sit non.'
          },
          v3:{
            title:'Fugit cupiditate alias nobis.',
            content:'Quam rem vitae est autem molestias explicabo debitis sint. Vero aliquid quidem commodi.'
          }
        }
      },
      bannerFourth:{
        header:'CONTACT US',
        location:{
          title:'Address',
          tip1:'Hangzhou',
          tip2:'Shangcheng'
        },
        phone:{
          title:'Phone Us',
          tip1:'18856635670',
          tip2:'xxxxxxxx'
        },
        message:{
          title:'Email',
          tip1:`468133459{'@'}qq.com`,
          tip2:''
        },
        timer:{
          title:'Business Hours',
          tip1:'All Day',
          tip2:'Monday to Saturday'
        },
        sendCustomName:'Enter your name',
        sendCustomEmail:'Enter your email address',
        sendCustomTheme:'Theme',
        sendCustomContent:'Please Enter Information',
        submit:'Sumbit'
      }
    },
    footer:{
      Name:'SiRuiCloud',
      Intro:`Cras fermentum odio eu feugiat lide par naso tierra. Justo eget nada terra videa magna
          derita valies darta donna mare fermentum iaculis eu non diam phasellus.`,
      userfulLink:'Friendly link',
      homepage:'Home',
      aboutus:'AboutUs',
      service:'Service',
      privacy:'Privacy',

      webSystemDevelopment:'webSystemDevelopment',
      threeJsDevelopment:'ThreeJsDevelopment',
      metacosmicDevelopment:'MetacosmicDevelopment',
      exhibition:'3DExhibition',
      digitalTwin:'DigitalTwin',

      contactUs:'ContactUs',
      location:'Shangcheng',
      city:'hangzhou',
      province:'Zhejiang',
      
      phone:'Phone',
      email:'Email',
    }
  }
   
  export default messages