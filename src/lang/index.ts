import { createI18n } from 'vue-i18n'
import en from './en'
import zh from './zh'
//引入的不同语言文件
const messages = {
  en, zh
}
const getCurrentLanguage = () => {
  if( localStorage.getItem('pageStatus')){
      return JSON.parse(localStorage.getItem('pageStatus') as string).recordPlatformLanguage
  }else{
      const language = navigator.language;
      const arr = language.split('-');
      return arr[0];
  }
}

//这个类型可以自己配置，毕竟每个人做的都不一样
const i18n:any =createI18n( {
  legacy: false,                                  // 使用 Composition API 模式，则需要将其设置为false
  globalInjection: true,                          //全局生效$t
  locale:getCurrentLanguage() || "zh",             // 默认zh翻译
  messages                                        //ES6解构
})
 
export default i18n;