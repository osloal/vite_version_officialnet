// import GlobalApi from "@src/js/GlobalApi";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";
import * as THREE from "three";

export default class ViewController {
    camera: THREE.PerspectiveCamera | null = null;
    controller: OrbitControls | null = null;

    cameraDir = 0;//相机方向
    cameraDirVertical = 0;//相机垂直方向
    cameraRate = 1;//相机移动速率
    isKeyPressing = {
        'left': false,
        'right': false,
        'up': false,
        'down': false,
    };//是否正在键盘按下状态


    //键盘控制视角方法
    keyFunctions: any = {
        //左
        "left": () => {
            if (!this.camera || !this.controller) return;
            let dir = this.cameraDir - Math.PI / 2;
            this.camera.position.x += this.cameraRate * Math.cos(dir);
            this.camera.position.z += this.cameraRate * Math.sin(dir);

            this.controller.target.x += this.cameraRate * Math.cos(dir);
            this.controller.target.z += this.cameraRate * Math.sin(dir);
        },
        //右
        "right": () => {
            if (!this.camera || !this.controller) return;
            let dir = this.cameraDir + Math.PI / 2;
            this.camera.position.x += this.cameraRate * Math.cos(dir);
            this.camera.position.z += this.cameraRate * Math.sin(dir);

            this.controller.target.x += this.cameraRate * Math.cos(dir);
            this.controller.target.z += this.cameraRate * Math.sin(dir);
        },
        //上
        "up": () => {
            if (!this.camera || !this.controller) return;
            this.camera.position.x += this.cameraRate * Math.cos(this.cameraDir);
            this.camera.position.z += this.cameraRate * Math.sin(this.cameraDir);

            this.controller.target.x += this.cameraRate * Math.cos(this.cameraDir);
            this.controller.target.z += this.cameraRate * Math.sin(this.cameraDir);

            // this.camera.position.y += this.cameraRate * Math.sin(this.cameraDirVertical);
            // this.controller.target.y += this.cameraRate * Math.sin(this.cameraDirVertical);
        },
        //下
        "down": () => {
            if (!this.camera || !this.controller) return;
            this.camera.position.x += -this.cameraRate * Math.cos(this.cameraDir);
            this.camera.position.z += -this.cameraRate * Math.sin(this.cameraDir);

            this.controller.target.x += -this.cameraRate * Math.cos(this.cameraDir);
            this.controller.target.z += -this.cameraRate * Math.sin(this.cameraDir);

            // this.camera.position.y += -this.cameraRate * Math.sin(this.cameraDirVertical);
            // this.controller.target.y += -this.cameraRate * Math.sin(this.cameraDirVertical);
        },
    }

    constructor(camera: THREE.PerspectiveCamera, controller: OrbitControls) {
        this.camera = camera;
        this.controller = controller;
    }


    onKeyDown(event: KeyboardEvent) {
        //键盘事件
        
        //====================== 相机移动 ==============================
        //若不是WASD或上下左右箭头按键,返回
        let keyCodeList = [38, 87, 37, 65, 40, 83, 39, 68];
        if (!keyCodeList.includes(event.keyCode)) return;


        //获取摄像机旋转值
        let rotationVector3 = new THREE.Vector3();
        this.camera!.getWorldDirection(rotationVector3);
        let spherical = new THREE.Spherical();
        spherical.setFromVector3(rotationVector3)
        //XOZ平面旋转值
        this.cameraDir = -spherical.theta + Math.PI / 2;
        //垂直方向旋转值
        this.cameraDirVertical = spherical.phi + Math.PI / 2;

        switch (event.keyCode) {
            case 38: // up
            case 87: // w
                if(this.isKeyPressing['up']) return;
                this.isKeyPressing['up'] = true;
                this.onKeyPress("up");
                break;
            case 37: // left
            case 65: // a
                if(this.isKeyPressing['left']) return;
                this.isKeyPressing['left'] = true;
                this.onKeyPress("left");
                break;
            case 40: // down
            case 83: // s
                if(this.isKeyPressing['down']) return;
                this.isKeyPressing['down'] = true;
                this.onKeyPress("down");
                break;
            case 39: // right
            case 68: // d
                if(this.isKeyPressing['right']) return;
                this.isKeyPressing['right'] = true;
                this.onKeyPress("right");
                break;
        }
        return true;
    }

    onKeyUp(event: KeyboardEvent) {
        switch (event.keyCode) {
            case 38: // up
            case 87: // w
                this.isKeyPressing['up'] = false;
                break;
            case 37: // left
            case 65: // a
                this.isKeyPressing['left'] = false;
                break;
            case 40: // down
            case 83: // s
                this.isKeyPressing['down'] = false;
                break;
            case 39: // right
            case 68: // d
                this.isKeyPressing['right'] = false;
                break;
        }
    }

    onKeyPress(direction: string) {

        this.keyFunctions[direction]();

        setTimeout(() => {
            if (this.isKeyPressing[direction]) {
                this.onKeyPress(direction)
            }
        }, 10)
    }
}