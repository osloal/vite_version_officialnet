import './Utils/Style/Global.less'
import { createApp } from 'vue';
import router from './router';
import App from './App.vue';
import './lib/rem.js'
import 'element-plus/dist/index.css'
import i18n  from './lang/index.js'  
import { againstDebugger , getIP } from './Utils/Global/Tools'
import {ElLoading} from 'element-plus'

import { createPinia } from 'pinia';
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate'
const pinia = createPinia();
pinia.use(piniaPluginPersistedstate);

(() => {
    const app = createApp(App);
    app.use(router);
    app.use(pinia);
    app.use(i18n);
    app.directive('loading', ElLoading.directive);
    app.mount('#app');
    console.log(
        '%c 🚀欢迎进入设计网站官网',
        'color:pink;font-size:20px;font-weight:bold;'
    );
    // againstDebugger();
    // getIP()
})()