// import {
//     Scene,
//     WebGLRenderer,
//     PerspectiveCamera,
//     AmbientLight,
//     GridHelper,
//     DirectionalLight,
//     Object3D,
// } from 'three';
import * as THREE from "three";
export default class GlobalConfig{
    public scene: THREE.Scene;
    public render: THREE.WebGLRenderer;
    public camera: THREE.PerspectiveCamera;
    public ambientLight: THREE.AmbientLight;
    public GridHelper: THREE.GridHelper;
    public derectionLight: THREE.DirectionalLight;
    public mouseRightUp: any;
    public mouseCurX: any = 0; // 当前鼠标位置
    public mouseCurY: any = 0;
    public mouseOldX: any = 0; // 上一次鼠标位置
    public mouseOldY: any = 0;
    public mouse: any;
    public mouseScreen: any;    //屏幕鼠标位置
    public raycaster: THREE.Raycaster | null = null;    //射线
    public floor: THREE.Mesh

    constructor(){
        this.mouseScreen = new THREE.Vector2();
        this.mouse = new THREE.Vector2();
        this.raycaster = new THREE.Raycaster();

        this.scene = new THREE.Scene();

        this.render = new THREE.WebGLRenderer({ antialias: true });
        this.render.setSize( window.innerWidth, window.innerHeight );

        this.camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 1000 );
        this.camera.position.x = 0;
        this.camera.position.y = 0;
        this.camera.position.z = 40;
        
        this.ambientLight = new THREE.AmbientLight(0xffffff);
        this.ambientLight.intensity = 0.8;

        this.GridHelper = new THREE.GridHelper( 1000, 1000, 0x808080, 0x444444 );
        this.GridHelper.position.y = -0.1;
        this.GridHelper.rotation.y = Math.PI/2;

        this.derectionLight = new THREE.DirectionalLight(0xffffff, 0.8);
        this.derectionLight.position.set(0, 0, 800);  

        // 三维平面鼠标拾取区域
        let plane = new THREE.PlaneGeometry(1000, 1000);
        let planeMaterial = new THREE.MeshBasicMaterial({color: 0x444444,opacity: 0, transparent: true ,depthTest:false});
        let planeMesh = new  THREE.Mesh(plane, planeMaterial);
        planeMesh.position.y = -0.1;
        planeMesh.rotation.x = -Math.PI/2;
        this.floor = planeMesh;

        // 坐标轴辅助线 x-red y-green z-blue
        // this.scene.add(new THREE.AxesHelper(1200));
    }
    public clearup() {
        while (this.scene.children.length > 0) {
            this.scene.remove(this.scene.children[0]);
        }
        this.scene.background = null;
    }
    public rendererCoord():void{
        this.scene.add( this.GridHelper );
        this.scene.add(this.floor);
    }
    public configCamera(){
        this.scene.add( this.camera );
    }
    public configAmbientLight(){
        this.scene.add(this.ambientLight)
    }
    onWindowResize(){
      this.camera.aspect = window.innerWidth / window.innerHeight;
      this.camera.updateProjectionMatrix();
      this.render.setSize( window.innerWidth, window.innerHeight );
    }
    public configDirectionalLight(){
        this.scene.add(this.derectionLight)
    }
    public mouseUpdate(e: any) {
        this.mouse.x = (e.clientX / window.innerWidth) * 2 - 1;
        this.mouse.y = -(e.clientY / window.innerHeight) * 2 + 1;
        this.mouseScreen.x = e.clientX;
        this.mouseScreen.y = e.clientY;
        var Intersections: any;
       
        this.raycaster!.setFromCamera(this.mouse, this.camera);
            Intersections = this.raycaster!.intersectObject(this.floor);
            for (let i = 0; i < Intersections.length; i++) {
                this.mouseCurX = Intersections[i].point.x;
                this.mouseCurY = -Intersections[i].point.z;
            }
    }
    public configTextLoader(url:string){
        let loader = new THREE.TextureLoader()
        let texture = loader.load(url)
        return texture
    }
//     public MouseDown(e: any) {
//         this.mouseUpdate(e);
//         if (e.button == 0) {
//             // if (this.mHouseClass.mouseDown(e, GlobalApi.g_mouseX, GlobalApi.g_mouseY)) {
//             //     GlobalApi.mCameraClass.mouseUp(e);
//             //     return true;
//             // }
//         //     GlobalApi.mCameraClass && GlobalApi.mCameraClass.mouseDown(e);
//         //     GlobalApi.childApp.mouseDown(e, GlobalApi.g_mouseX, GlobalApi.g_mouseY); // 黄少良
//         // }
//         // if (GlobalApi.currentMode === "3D") {
//         //     GlobalApi.mHouseClass.mLouCengArray[0].mNeiQiangClass.OnPick3D();
//         // }
//     }
// }

    // public mouseMove(e:any){
    //     this.mouseUpdate(e);
    // }

//     public MouseUp(e:any){
//         this.mouseUpdate(e);
//     }

}
