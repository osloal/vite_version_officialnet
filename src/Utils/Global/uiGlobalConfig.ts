const initWebStatusConfig = {
    //获取当前运行平台的语言
    getPlatformLanguage: ()=>{
        if(localStorage.getItem('lang')){
            return localStorage.getItem('lang') as string;
        }else{
            const language = navigator.language;
            const arr = language.split('-');
            localStorage.setItem('lang', arr[0]);
            return arr[0];
        }
    }
}
interface yWebStatusConfig {
    webBrowserLanguage:string
}

export const yWebStatusConfig: yWebStatusConfig = {
    webBrowserLanguage: initWebStatusConfig.getPlatformLanguage(),
}