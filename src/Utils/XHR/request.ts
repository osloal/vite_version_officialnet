import axios from 'axios'
import { ElMessage } from 'element-plus'

const service = axios.create({
    baseURL:import.meta.env.VITE_BASE_API_URL,
    withCredentials:false,//异步请求携带cookie
    timeout:10*1000,
    headers:{"content-type":"application/json"}
})

//配置请求拦截器
service.interceptors.request.use(config => {
    config.headers.Authorization = localStorage.getItem("token") || '';
    config.headers['Content-Type'] = 'application/json';
    return config
})

//配置响应拦截器
service.interceptors.response.use(response =>{
    if(response.status == 200){
        if(response.data.code === "1"){
            ElMessage({
                type:'success',
                message:response.data.msg,
                duration:1500
            })
            return response.data
        }else{
            ElMessage({
                message:response.data.msg || '请求失败',
                type:'error',
                duration:1500
            })
            return response.data
        }
    }
},error => {
    ElMessage({
        message:'网络错误' || '服务器内部错误',
        type:'error'
    })
})

export default service