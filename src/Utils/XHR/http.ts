import request from './request'

interface http {
    get(url:string,params:any):void,
    post(url:string,params:any):void
}

const http:http = {
    get(url,params){
        const config = {
            method:'GET',
            url:url,
            data:params ? params :{},
        }
        return request(config)
    },
    post(url,params){
        const config = {
        method:'POST',
            url:url,
            data:params ? params :{},
        }
        return request(config)
    }
}
export default http