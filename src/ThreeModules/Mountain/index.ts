import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js'
import { RenderPass } from 'three/examples/jsm/postprocessing/RenderPass.js';
import { UnrealBloomPass } from 'three/examples/jsm/postprocessing/UnrealBloomPass.js';
import { EffectComposer } from 'three/examples/jsm/postprocessing/EffectComposer.js';
import { RGBELoader } from 'three/examples/jsm/loaders/RGBELoader.js';
import GlobalConfig from '../../Utils/Global/threeGlobalConfig';
import * as THREE from "three";

import { GUI } from 'lil-gui';
import { ElLoading } from 'element-plus'
import {GLTFLoader} from 'three/examples/jsm/loaders/GLTFLoader.js';
import { ViewHelper } from '../../lib/ViewHelper';
import { pageStatusStore } from '../../store/pageStatus';
import Transformer3D from "../../lib/transformer3D";

import { GLTFExporter } from "three/examples/jsm/exporters/GLTFExporter.js";
import { DRACOLoader } from "three/examples/jsm/loaders/DRACOLoader.js";
import { log } from 'three/examples/jsm/nodes/Nodes.js';
import  ViewController  from '../../lib/viewController/viewController';
import imgWater from '@/assets/Models/Mountain/waternormals.jpg'

// 导入水面
// import Water from '../../../node_modules/three/examples/jsm/objects/Water2.js'
import {Water} from 'three/examples/jsm/objects/Water2.js'

const modelStatus = pageStatusStore();

class Mountain extends GlobalConfig {
    public water:any
    public controls: OrbitControls;
    private outLight: RenderPass;
    private bloomPass: UnrealBloomPass;
    private composer: EffectComposer;
    private viewControl: ViewController | null;
    public helper:ViewHelper|null;
    private object: any;
    public Loading: any;
    // public pickFlag:boolean;                    //判断是否有被选中的物体
    public sphere:any
    public tsf:any                  //控制器
    
    // public transform3D:Transformer3D
    private params = {
        exposure: 1,
        bloomStrength: 0.7,
        bloomThreshold: 0,
        bloomRadius: 0.6
    }
    private static path_all_01 = [
        'models/Mountain/ts/ts_test.gltf',
        'models/Mountain/ss/ss_test01.gltf',
        'models/Mountain/hs/hs_test.gltf',
        'models/Mountain/hns/hns_test.gltf',
    ]
    private static path_all_02=[
        'models/Mountain/ts/ts_test.gltf',
        'models/Mountain/ss/ss_test02.gltf',
        'models/Mountain/hs/hs_test.gltf',
        'models/Mountain/hns/hns_test.gltf',
    ];
    private gui = {
        mountainX:180, 
        mountainY:0.5, 
        mountainZ:120, 
        mountainScale:0.15,
        model_path:'models/Mountain/ss/ss_test01.gltf',
        _this:this,
        // 导出模型
        导出模型(){
            this._this.exportGlb()
        },
        //泰山01
        createTs_01(){
            this.model_path = Mountain.path_all_01[0];
            this._this.scene.remove(this._this.object);
            this._this.initModel();
        },
        //嵩山01
        createSs_01() {
            this.model_path = Mountain.path_all_01[1];
            this._this.scene.remove(this._this.object);
            this._this.initModel();
        },
        //恒山01
        createHns_01: function () {
            this.model_path = Mountain.path_all_01[2];
            this._this.scene.remove(this._this.object);
            this._this.initModel();
        },
        //衡山01
        createHs_01: function () {
            this.model_path = Mountain.path_all_01[3];
            this._this.scene.remove(this._this.object);
            this._this.initModel();
        },

        //泰山02
        createTs_02: function () {
            this.model_path = Mountain.path_all_02[0];
            this._this.scene.remove(this._this.object);
            this._this.initModel();
        },
        //嵩山02
        createSs_02: function () {
            this.model_path = Mountain.path_all_02[1];
            this._this.scene.remove(this._this.object);
            this._this.initModel();
        },
        //恒山02
        createHns_02: function () {
            this.model_path = Mountain.path_all_02[2];
            this._this.scene.remove(this._this.object);
            this._this.initModel();
        },
        //衡山02
        createHs_02: function () {
            this.model_path = Mountain.path_all_02[3];
            this._this.scene.remove(this._this.object);
            this._this.initModel();
        },
    };
    constructor() {
        super()
        this.rendererCoord();
        this.configCamera();
        this.configAmbientLight();
        this.configDirectionalLight();
        modelStatus.modelLoadingStatus = true;
        this.controls = new OrbitControls(this.camera, this.render.domElement);
        this.controls.maxPolarAngle = Math.PI * 0.5;
        this.controls.minDistance = 1;
        this.controls.maxDistance = 2000;
        this.controls.autoRotate = true;
        this.controls.autoRotateSpeed = 0.5;
        this.controls.enablePan = true;
        // this.transform3D = new Transformer3D(this.object, this.scene, this.camera)
        //更新环境光
        this.ambientLight.intensity = 0.45;
        this.ambientLight.position.set(0, 0, 800);

        this.helper = null;

        //设置外发光
        this.outLight = new RenderPass(this.scene, this.camera);
        this.bloomPass = new UnrealBloomPass(new THREE.Vector2(window.innerWidth, window.innerHeight), 1.5, 0.4, 0.85);
        this.bloomPass.threshold = this.params.bloomThreshold;
        this.bloomPass.strength = this.params.bloomStrength;
        this.bloomPass.radius = this.params.bloomRadius;

        this.composer = new EffectComposer(this.render);
        this.composer.addPass(this.outLight);
        this.composer.addPass(this.bloomPass);
        // 相机移动控制器
        this.viewControl = new ViewController(this.camera, this.controls);

        //增加hdr贴图
        this.changeEnvMap('https://3d.shixianjia.com/hdr_sky/hdr/pizzo_pernice_puresky_4k.hdr')
        this.initModel()
        this.initWater()
        this.initGUI()
        this.animate()
        //在初始化的时候添加事件监听
        this.render.domElement.addEventListener('mousedown', this.mouseDown.bind(this), false);
        this.render.domElement.addEventListener('mousemove', this.mouseMove.bind(this), false);
        this.render.domElement.addEventListener('mouseup', this.mouseUp.bind(this), false);
        //监听事件
        document.addEventListener( 'keydown', this.onKeyDown, false );
		document.addEventListener( 'keyup', this.onKeyUp, false );

        /*
            YOUR CODE ...
        */  
        // this.pickFlag = false;

    }
    private animate(){                                        
        requestAnimationFrame(() => this.animate());
        this.render.render(this.scene, this.camera);
        this.helper?.render();
    }
    private changeEnvMap(url: string) {
        new RGBELoader().setDataType(THREE.HalfFloatType).load(
            url,
            t => {
                t.mapping = THREE.EquirectangularReflectionMapping;
                this.scene.background = t;
            },
        );
    }
    private initGUI(){
        let _this = this
        const datGui = new GUI()
        //山体材质01的操作
        var objectFolder = datGui.addFolder("山体材质01");
        objectFolder.add(this.gui,"mountainX",-250,250).onChange(function (e:any) {
            _this.object.position.x = e;
        });
        objectFolder.add(this.gui,"mountainY",0,50).onChange(function (e:any) {
            _this.object.position.y = e;
        });
        objectFolder.add(this.gui,"mountainZ",-250,250).onChange(function (e:any) {
            _this.object.position.z = e;
        });
        objectFolder.add(this.gui,"mountainScale",0.01,1).onChange(function (e:any) {
            _this.object.scale.set(e, e, e);
        });
        objectFolder.add(this.gui, "导出模型");
        objectFolder.add(this.gui, "createTs_01");
        objectFolder.add(this.gui, "createSs_01");
        objectFolder.add(this.gui, "createHns_01");
        objectFolder.add(this.gui, "createHs_01");
        
        //山体材质02的操作
        var objectFolder = datGui.addFolder("山体材质02");
      
        objectFolder.add(this.gui, "createTs_02");
        objectFolder.add(this.gui, "createSs_02");
        objectFolder.add(this.gui, "createHns_02");
        objectFolder.add(this.gui, "createHs_02");
    }
    public removeGUI(){
        let GUIDom:HTMLElement|null = document.querySelector('.lil-gui')
        if(GUIDom){
            GUIDom.remove()
        }
    }

    // 创建水面
    private initWater(){

        const waterGeometry = new THREE.CircleGeometry(1000,64)
        let water = new Water(waterGeometry,{
            textureWidth:1024,
            textureHeight:1024,
            color:0xeeeeff,
            flowDirection:new THREE.Vector2(1,1),
            scale:1,
            flowSpeed:0.03,
            // 一定要加贴图，不然会报错
            normalMap0:new THREE.TextureLoader().load(imgWater,function ( texture ) {texture.wrapS = texture.wrapT = THREE.RepeatWrapping;}),
            normalMap1:new THREE.TextureLoader().load(imgWater,function ( texture ) {texture.wrapS = texture.wrapT = THREE.RepeatWrapping;}),
        })
    
        water.rotateX(-Math.PI/2)
        water.position.set(0,1,0)
        water.name = 'water'
        this.scene.add(water)
    }

    private initModel() {

        let _this = this
        // 实例化解压库
        let dracoLoader = new DRACOLoader();
        dracoLoader.setDecoderPath(import.meta.env.VITE_BASE_API_URL)
        
        var loader = new GLTFLoader();
        this.Loading = ElLoading.service({
            lock: true,
            text: '正在加载模型',
            background: 'rgba(0, 0, 0, 0.7)',
        })
        loader.setDRACOLoader(dracoLoader);
        loader.load(
            import.meta.env.VITE_BASE_API_URL+this.gui.model_path, 
            // import.meta.env.VITE_BASE_API_URL+this.gui.model_path, 
            // "./src/assets/models/Mountain/ss/file.glb",
            // "./src/assets/models/Mountain/ss/ss_test01.gltf",

            gltf => {
                _this.object = gltf.scene;
                _this.object.name='shanti'
                if(modelStatus.modelLoadingStatus){
                    _this.scene.add(_this.object);
                    _this.object.scale.set(0.13,0.13,0.13); 
                    _this.object.position.set(0,6,0);
                    // window.Mountain = _this;
                }else{
                    return false
                }
            },
            function(xhr){
                const progress = Math.floor(xhr.loaded / xhr.total * 100);
                if(modelStatus.modelLoadingStatus){
                    _this.onLoadingMask(progress)
                    if(xhr.loaded / xhr.total * 100 == 100){
                        _this.offLoadingMask()
                    }
                }else{
                    _this.offLoadingMask()
                    // window.Mountain = _this;
                }
            },
            (error) => {
                console.error('模型终止加载或资源异常:', error);
            }
        );
    }
    public cleanup(){
       this.clearup()
       //在清除场景的时候，移除事件监听
       this.render.domElement.removeEventListener('mousedown', this.mouseDown.bind(this), false);
       this.render.domElement.removeEventListener('mousemove', this.mouseMove.bind(this), false);
       this.render.domElement.removeEventListener('mouseup', this.mouseUp.bind(this), false);
       
    }
    public onLoadingMask(progress:number){
        
        this.Loading.setText('正在加载模型：['+progress+'%]');
    }
    public offLoadingMask(){
        this.Loading.close()
    }
    private mouseDown(e:any){
            e.preventDefault();
            this.mouseUpdate(e);
            // 防止双击控制器moused事件触发
            if(this.tsf?.controller_3d){
                if(this.tsf?.mousedown(e)){
                    return true;
                }
            }
            this.tsf?.release();
			this.tsf = undefined

              let intersects = this.raycaster!.intersectObject(this.object); //射线与模型相交的数组
              if (intersects.length > 0) {
                this.controls.enabled = false;
                // this.pickFlag = true;
                console.log('山体点击')

                let cabinetData={
					m_Object3D:this.object,
					isLock:false,
					setLock:(isLock:boolean)=>{},
					destory:()=>{},
					setHighLight:(val:boolean)=>{},
					setVisible:(isV:boolean)=>{},
					// move:(vect3:THREE.Vector3)=>{},
					// rotate:(vect3:THREE.Vector3)=>{},
					mirrorX:()=>{},
					mirrorY:()=>{},
					mirrorZ:()=>{},
					copy:()=>{}
				}
                let cameraData={ m_Camera3D: this.camera, m_Control3D: this.controls }
				this.tsf = new Transformer3D(cabinetData, this.scene, cameraData, 'normal')
              }else{
                this.tsf?.destory()
              }
              
    }

    private mouseMove(e:any){  
        e.preventDefault();
        this.mouseUpdate(e);
        if (this.tsf) {
            this.tsf.mousemove(e)
            this.controls.enableRotate = false
          } else {
            this.controls.enableRotate = true
          }

        // if(this.pickFlag){
        //     this.object.position.x = this.mouseCurX;
        //     this.object.position.z = -this.mouseCurY;
        // }
       this.mouseOldX = this.mouseCurX;
       this.mouseOldY = this.mouseCurY;
        
// this.sphere.position.set(this.mouseCurX,0,-this.mouseCurY)      //开启小球检测


    }

    private mouseUp(e:any){  
        e.preventDefault();
        this.controls.enabled = true;
        // this.pickFlag = false;    
        this.tsf && this.tsf.mouseup(e)
    }
    private onKeyDown = (evt: any)=> {
        let e = window.event || evt;
        return this.viewControl!.onKeyDown(e);
    }
    private onKeyUp = (evt: any)=> {
        return this.viewControl!.onKeyUp(evt);
    }
    private exportGlb() {
        
        console.log('导出模型')
        let exporter = new GLTFExporter();
        
        
                let res = new THREE.Group();        //需要导出的组
        
                let objs:any = [];                               //防止过滤器影响当前场景
        
                this.scene.children.forEach((item:any) => {
                    objs.push(item.clone());
        
                });
        
                let resList = objs.filter((ele: any) => {
                    return (
                        // (整个导出过程过滤是最重要的，过滤条件自己判断需要什么类型的scene中的chdren，最方便的就是给需要导出的模型或者物体加入name或者userData)
                        //根据自身需求，加限制，进行过滤!(如果是mesh必须有贴图!!!)      
        
                        // (ele instanceof (THREE.Group) || ele instanceof (THREE.Mesh))
                        (ele instanceof (THREE.Group))
        
                        &&
                        (ele.name = 'shanti')
                        // ((ele as THREE.Mesh).material && ((ele as THREE.Mesh).material as any).map && (((ele as THREE.Mesh).material as any).map != null))        //如果是mesh必须有贴图!!!
        
                    );
        
                })
        
                resList && res.add(...resList)
        
               
        
        exporter.parse(res, (ele: any) => {
        
                     //下载
        
                     const objFileData = new Blob([ele], { type: 'application/octet-stream' });
        
                     //下载文件
        
                     window.open(URL.createObjectURL(objFileData));
        
                     const a = document.createElement('a');
        
                     a.href = URL.createObjectURL(objFileData);
        
                     a.download = 'file.glb';
        
                     a.click();
        
                 }, (err: any) => { console.log(err) },
        
                     {
        
                         binary: true,
        
                     }
        )
                return;
        
        }
}
export default Mountain 