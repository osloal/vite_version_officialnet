	var camera, scene, renderer, controls,mAmbientLight,mLightPos,light;
	var objects = [];
	var raycaster;
	var mouse       = new THREE.Vector2(), INTERSECTED;
	var moveForward  = false;
	var moveBackward = false;
	var moveLeft 	 = false;
	var moveRight 	 = false;
	var canJump 	 = false;

	var prevTime = performance.now();
	var velocity = new THREE.Vector3();
	var direction = new THREE.Vector3();
	var vertex = new THREE.Vector3();
	var color = new THREE.Color();

	var bCollision = true;
	//控制按钮
			init();
			animate();
			var m_fSpeed = 1000;
			
			function init() {
	
			camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 1, 50000 );
			camera.position.x = 0;
			camera.position.y = 160;
			camera.position.z = 0;
			scene = new THREE.Scene();
			
					
					
			mAmbientLight = new THREE.AmbientLight(0xffffff);
			mAmbientLight.intensity = 0.65;
			mAmbientLight.position.set(0, 0, 800); 								
			scene.add(mAmbientLight);
			

	
			var bumpTex = new THREE.TextureLoader().load( './img/glass_at.jpg' );
			var normalTex = new THREE.TextureLoader().load( './img/glass_nm.jpg' );
			var texture1 = new THREE.TextureLoader().load('./img/glass.jpg');
			texture1.wrapS = texture1.wrapT = THREE.RepeatWrapping;
			texture1.repeat.set(64, 64);
			var groundGeo = new THREE.PlaneBufferGeometry( 50000, 50000 );
			var groundMat = new THREE.MeshLambertMaterial( {
				color: 0x999999,
				// lightMap: texture1,//光照贴图
				map: texture1,//贴图
				// side:
					bumpMap: bumpTex,
					bumpScale: 0.3,
					roughness: 0.3,
					normalMap:normalTex,
			 } );
			var ground = new THREE.Mesh( groundGeo, groundMat );
			ground.rotation.x = - Math.PI / 2;
			ground.position.y = -2;
			// 地面接受阴影
			ground.receiveShadow = true;
			scene.add( ground );
			
			
			
			controls = new THREE.PointerLockControls( camera );
	
			var app_pc 	 = document.getElementById( 'app_pc' );
			var blocker 	 = document.getElementById( 'blocker' );
			var instructions = document.getElementById( 'instructions' );
			app_pc.style.display="block";
			instructions.addEventListener( 'click', function () {
				controls.lock();
			}, false );
			controls.addEventListener( 'lock', function () {
				instructions.style.display = 'none';
				blocker.style.display = 'none';
				moveForward  = false;
				moveLeft 	 = false;
				moveBackward = false;
				moveRight 	 = false;
				velocity.x   = 0;
				velocity.y   = 0;
				velocity.z   = 0;
			} );
	
			controls.addEventListener( 'unlock', function () {
				blocker.style.display = 'block';
				instructions.style.display = '';
			} );
	
			scene.add( controls.getObject() );
			
			var onKeyDown = function ( event ) {
	
				switch ( event.keyCode ) {
					case 38: // up
					case 87: // w
						moveForward = true;
	
						break;
	
					case 37: // left
					case 65: // a
						moveLeft = true;
						break;
	
					case 40: // down
					case 83: // s
						moveBackward = true;
						break;
	
					case 39: // right
					case 68: // d
						moveRight = true;
						break;
	
					case 32: // space
						if ( canJump === true ) 
						{
					/*		if(g_fFloor == 0) // 向上跳
								velocity.y += 1000;
							else*/		
								velocity.y += 350;	//向下跳
							g_fFloor = 0;
						}
						canJump = false;
						break;
					
					case 78://n
					enter();
					break;
				}
			};
	

			var onKeyUp = function ( event ) {
				
				switch ( event.keyCode ) {
					case 38: // up
					case 87: // w
						moveForward = false;
						break;
					case 37: // left
					case 65: // a
						moveLeft = false;
						break;
					case 40: // down
					case 83: // s
						moveBackward = false;
						break;
					case 39: // right
					case 68: // d
						moveRight = false;
						break;
					case 82: // 'R'
					{
						if( bCollision == false )
						{
							   bCollision = true;
							   $('#myText').html("漫游模式已打开,按R键切换");
							   canJump = true;
						   }
						   else
						   {
							   bCollision = false;
							   $('#myText').html("飞行模式已打开,,按R键切换");
						   }
					}
					break;					
				}
			};
	
			document.addEventListener( 'keydown', onKeyDown, false );
			document.addEventListener( 'keyup', onKeyUp, false );
	
			raycaster = new THREE.Raycaster( new THREE.Vector3(), new THREE.Vector3( 0, - 1, 0 ), 0, 10 );
	
			renderer = new THREE.WebGLRenderer( { antialias: true } );
			renderer.setPixelRatio( window.devicePixelRatio );
			renderer.setSize( window.innerWidth, window.innerHeight );
			// 渲染器开启阴影
			renderer.shadowMap.enabled = true;
			renderer.shadowMap.type = THREE.PCFSoftShadowMap; // default
					light = new THREE.DirectionalLight( 0xffffff, 0.6 );
					// 此属性设置为 true 方向光灯将投射阴影。
					light.castShadow = true;
					// 分辨率降低，会显得影子柔和
					light.shadow.mapSize.width = 512; 
					light.shadow.mapSize.height = 512;
					light.shadow.camera.near = -10000; 
					light.shadow.camera.far = 50000; 
					light.shadow.camera.left = -2000; 
					light.shadow.camera.right = 2000; 
					light.shadow.camera.top = 2000; 
					light.shadow.camera.bottom = -2000; 
					scene.add( light );
					// Skybox
	
					var sky = new THREE.Sky();
	
					var uniforms = sky.material.uniforms;
	
					uniforms[ 'turbidity' ].value = 10;
					uniforms[ 'rayleigh' ].value = 2;
					uniforms[ 'luminance' ].value = 1;
					uniforms[ 'mieCoefficient' ].value = 0.005;
					uniforms[ 'mieDirectionalG' ].value = 0.8;
	
					var parameters = {
						distance: 400,
						// inclination: 0.49,
						// azimuth: 0.205
						inclination: 0.15,
						// '太阳倾向': 0.15,
						azimuth: 0.205,
						// '太阳方位角': 0.205
					};
	
					var cubeCamera = new THREE.CubeCamera( 0.1, 1, 512 );
					cubeCamera.renderTarget.texture.generateMipmaps = true;
					cubeCamera.renderTarget.texture.minFilter = THREE.LinearMipMapLinearFilter;
	
					scene.background = cubeCamera.renderTarget;
	
					function updateSun() {
	
						var theta = Math.PI * ( parameters.inclination - 0.84 );
						var phi = 2 * Math.PI * ( parameters.azimuth - 0.5 );
	
						light.position.x = parameters.distance * Math.cos( phi );
						light.position.y = parameters.distance * Math.sin( phi ) * Math.sin( theta );
						light.position.z = parameters.distance * Math.sin( phi ) * Math.cos( theta );
	
						sky.material.uniforms[ 'sunPosition' ].value = light.position.copy( light.position );
						// water.material.uniforms[ 'sunDirection' ].value.copy( light.position ).normalize();
	
						cubeCamera.update( renderer, sky );
	
					}
						//调用太阳跟新
						updateSun();

			container_pc.appendChild( renderer.domElement );
			window.addEventListener( 'resize', onWindowResize, false );
			window.addEventListener('mousemove', mouseMove,  false );
			
			var gui = new dat.GUI( {  width: 280,autoPlace: false } );
			var customContainer = document.getElementById('myGui');
				customContainer.appendChild(gui.domElement);		
	
			gui.open();
			param = {'移动速度': 1500,
					 '视野': 60,
					 '环境光': 1,
					 '镜头光': 200,
					 '太阳倾向':0.15,
					 '太阳方位角':0.205
					};
	
			var lightFolder = gui.addFolder( '控制窗口' );
			lightFolder.add( param, '移动速度',500,2000 ).step( 10 ).onChange( function ( val ) {
				m_fSpeed = val;
			} );
			lightFolder.add( param, '视野', 45, 120 ).step( 1 ).onChange( function ( val ) {
				camera.fov = val;
				camera.updateProjectionMatrix();
			} );
			lightFolder.add( param, '环境光', 0.3, 3 ).step( 0.01 ).onChange( function ( val ) {
				mAmbientLight.intensity = val;
			} );
			
			lightFolder.open();		
			//天空
			var folder = gui.addFolder( '天空' );
			folder.add( param, '太阳倾向', -0.16, 0.5, 0.0001 ).onChange( function(inclination){
				parameters.inclination = inclination;
				updateSun();
			} );
			folder.add( param, '太阳方位角', 0, 1, 0.0001 ).onChange( function(azimuth){
				parameters.azimuth = azimuth;
				updateSun();
			} );
					folder.open();	
		}
		
		function mouseMove(e)
		{ 
		//	event.preventDefault();
			mouse.x = ( e.clientX / window.innerWidth ) * 2 - 1;
			mouse.y = - ( (e.clientY-0) / window.innerHeight ) * 2 + 1;
		}
	
	
		function onWindowResize() {
			camera.aspect = window.innerWidth / window.innerHeight;
			camera.updateProjectionMatrix();
			renderer.setSize( window.innerWidth, window.innerHeight );
		}
	 
		var g_fFloor = 0;
		var distance = 110;
		var direction1 = new THREE.Vector3();
		function animate() {
			TWEEN.update();
			requestAnimationFrame( animate );
	
			if ( controls.isLocked === true ) {
	
				// 向一个方向移动
	//============================================================================================================
				camera.getWorldDirection( direction1 );
	//============================================================================================================
	
				raycaster.ray.origin.copy( controls.getObject().position );
				raycaster.ray.origin.y -= 10;
	
				var intersections = raycaster.intersectObjects( objects );
	
				var onObject = intersections.length > 0;	
	
				var time = performance.now();
	
				var delta = ( time - prevTime ) / m_fSpeed;
	
				
				velocity.x -= velocity.x * 10.0 * delta;
				velocity.z -= velocity.z * 10.0 * delta;
				velocity.y -= 9.8 * 100.0 * delta; // 100.0 = mass
				
				direction.z = Number( moveForward ) - Number( moveBackward );
				direction.x = Number( moveLeft ) 	- Number( moveRight );
				direction.normalize();	
									
				if ( onObject == true && bCollision ==true) {
						
					controls.getObject().translateX( velocity.x * delta );
					controls.getObject().position.y += ( velocity.y * delta ); // new behavior
					controls.getObject().translateZ( velocity.z * delta );
				
					prevTime = time;
					renderer.render( scene, camera );
					return;		
				}
				else
				{
					if ( moveForward || moveBackward ) velocity.z -= direction.z * 4000.0 * delta;
					if ( moveLeft || moveRight ) 	   velocity.x -= direction.x * 4000.0 * delta;	
				}
				
				controls.getObject().translateX( velocity.x * delta );
				controls.getObject().translateZ( velocity.z * delta );
				if(bCollision == true)
				{
					controls.getObject().position.y += ( velocity.y * delta ); // new behavior					
				}
	
				if ( controls.getObject().position.y < g_fFloor+160 ) {
					velocity.y = 0;
					controls.getObject().position.y = g_fFloor+160;
					canJump = true;
				}
	
				prevTime = time;
			}
	
			renderer.render( scene, camera );
		}

