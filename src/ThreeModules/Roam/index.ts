import {PointerLockControls} from 'three/examples/jsm/controls/PointerLockControls.js'
// import * as TWEEN from '@tween.js/tween.js'
import GlobalConfig from '../../Utils/Global/threeGlobalConfig';
import * as THREE from 'three';
import { GUI } from 'lil-gui';
import { ElLoading } from 'element-plus'
import {GLTFLoader} from 'three/examples/jsm/loaders/GLTFLoader.js';
import { ViewHelper } from '../../lib/ViewHelper';
import { pageStatusStore } from '../../store/pageStatus';
import {Sky} from 'three/examples/jsm/objects/Sky.js'
import $ from "jquery"
import glass_01 from '@/assets/Roam/groundMap/glass_at.jpg'
import glass_02 from '@/assets/Roam/groundMap/glass_nm.jpg'
import glass_03 from '@/assets/Roam/groundMap/glass.jpg'
const modelStatus = pageStatusStore();

class Roam extends GlobalConfig {
    public controls: PointerLockControls;   //控制器
    // public helper:ViewHelper|null;
    // private object: any;
    // public Loading: any;

    public moveForward:boolean;   //移动的相关变量
    public moveBackward:boolean;
    public moveLeft:boolean;
    public moveRight:boolean;
    public canJump:boolean;
    public velocity:any = new THREE.Vector3(); //移动的速度变量

    public m_fSpeed:number = 1000; //控制器的移动速度
    public g_fFloor:number = 0;     
    public bCollision:boolean = true
    public mouse:any = new THREE.Vector2(); 
    public parameters = {
        distance: 400,
        // inclination: 0.15,
        turbidity: 10,
        rayleigh: 2,
        mieCoefficient: 0.005,
        mieDirectionalG: 0.8,
        elevation: 0.15,     // '太阳倾向': 0.15,
        azimuth: 0.205,    // '太阳方位角': 0.205
        // exposure: this.render.toneMappingExposure
    }
    public sky:any =  new Sky()
    public cubeCamera:any;
    public distance:number = 110;
    public direction1 = new THREE.Vector3();
    public direction = new THREE.Vector3()
    public mLightPos = new THREE.PointLight(0xffffff, 1, 260 )   //镜头光
    public objects = []
    public prevTime = performance.now();
    public sun = new THREE.Vector3()
    private static path_all_01 = [
        glass_01,glass_02,glass_03
    ];
 
    private gui = {
        '移动速度': 1500,
        '视野': 60,
        '环境光': 1,
        '镜头光': 200,
        '太阳倾向':0.15,
        '太阳方位角':0.205,
    }

    constructor() {
        super()
        // this.rendererCoord();
        // 摄像机
        this.configCamera();
        this.camera.position.set(0, 160, 0);
        // 环境光源
        this.configAmbientLight();
        this.ambientLight.position.set(0, 0, 800);
        this.ambientLight.intensity = 0.65;
        // 方向光
        this.configDirectionalLight();
        this.derectionLight.castShadow = true;
        this.derectionLight.shadow.mapSize.width = 512;
        this.derectionLight.shadow.mapSize.height = 512;
        this.derectionLight.shadow.camera.near = -10000; 
        this.derectionLight.shadow.camera.far = 50000; 
        this.derectionLight.shadow.camera.left = -2000; 
        this.derectionLight.shadow.camera.right = 2000; 
        this.derectionLight.shadow.camera.top = 2000; 
        this.derectionLight.shadow.camera.bottom = -2000;
        // 设置镜头光位置
        this.mLightPos.position.x = 0;
        this.mLightPos.position.y = 0;
        this.mLightPos.position.z = 0;
        
        let bumpTex = this.configTextLoader( Roam.path_all_01[0] );// 加载凹凸贴图
        let normalTex = this.configTextLoader( Roam.path_all_01[1] );//加载法向贴图
        let texture1 = this.configTextLoader( Roam.path_all_01[2] );//加载材质纹理
        texture1.wrapS = texture1.wrapT = THREE.RepeatWrapping;  //设置材质纹理水平垂直方向重复平铺
        texture1.repeat.set( 64, 64 );

        let groundGeo = new THREE.PlaneGeometry( 50000, 50000 );
        let groundMat = new THREE.MeshLambertMaterial( {
            color: 0x999999,
            // lightMap: texture1,//光照贴图
            map: texture1,//贴图
            // side:
            bumpMap: bumpTex,
            bumpScale: 0.3,
            // roughness: 0.3,
            normalMap:normalTex,
        } );
        let ground = new THREE.Mesh( groundGeo, groundMat );
        ground.rotation.x = - Math.PI / 2;
        ground.position.y = -2;
        // 地面接受阴影
        ground.receiveShadow = true;
        this.scene.add( ground );

        this.controls = new PointerLockControls( this.camera,this.render.domElement ); //第一视角控制器

        this.cubeCamera = new THREE.CubeCamera( 0.1, 1,  new THREE.WebGLCubeRenderTarget( 512, { generateMipmaps: true, minFilter: THREE.LinearMipmapLinearFilter } ));
        this.cubeCamera.renderTarget.texture.generateMipmaps = true;
        this.cubeCamera.renderTarget.texture.minFilter = THREE.LinearMipMapLinearFilter;

        this.scene.background = this.cubeCamera.renderTarget;

        this.render.setPixelRatio(window.devicePixelRatio);
	    // 渲染器开启阴影
        this.render.shadowMap.enabled = true
        this.render.shadowMap.type = THREE.PCFSoftShadowMap; // default
        this.render.toneMapping = THREE.ACESFilmicToneMapping;
		this.render.toneMappingExposure = 0.5;

        this.raycaster = new THREE.Raycaster(new THREE.Vector3(),new THREE.Vector3(0,-1,0),0,10)

        this.moveForward  = false;
        this.moveBackward = false;
        this.moveLeft 	 = false;
        this.moveRight 	 = false;
        this.canJump 	 = false;

        this.sky.scale.setScalar( 450000 )
        this.scene.add(this.sky)
        this.updateSun()
        this.scene.add( this.controls.getObject() );

        this.initGUI()
        this.animate()

        //监听事件
        document.addEventListener( 'keydown', this.onKeyDown, false );
		document.addEventListener( 'keyup', this.onKeyUp, false );
        console.log(this.controls)

        let container_pc = document.getElementById( 'container_pc' )
        container_pc?.appendChild(this.render.domElement)
        // 监听dom元素的点击事件，如果点击了，激活控制器指针锁定
        document.addEventListener( 'click',this.lockedControls_first, false );
        //当指针状态为 “locked” （即鼠标被捕获）时触发
        this.controls.addEventListener( 'lock', this.lockedControls_second);
        //当指针状态为 “unlocked” （即鼠标不再被捕获）时触发
        this.controls.addEventListener( 'unlock', this.unlocked );
    
        this.scene.add( this.controls.getObject() );

    }

    public lockedControls_first = () =>{
        this.controls.lock();
    }

    public lockedControls_second = () =>{
        let instructions = document.getElementById( 'instructions' );

        if(instructions){
            instructions.style.display = 'none';

        }
        this.moveForward  = false;
        this.moveLeft 	 = false;
        this.moveBackward = false;
        this.moveRight 	 = false;
        this.velocity.x   = 0;
        this.velocity.y   = 0;
        this.velocity.z   = 0;
    }

    private unlocked = () =>{
        let instructions = document.getElementById( 'instructions' );
        if(instructions){
            instructions.style.display = '';
        }
    }

    private onKeyDown = (event:any) =>{
        switch ( event.keyCode ) {
            case 38: // up
            case 87: // w
                this.moveForward = true;
                break;

            case 37: // left
            case 65: // a
                this.moveLeft = true;
                break;

            case 40: // down
            case 83: // s
                this.moveBackward = true;
                break;

            case 39: // right
            case 68: // d
                this.moveRight = true;
                break;

            case 32: // space
                if ( this.canJump === true ) 
                {
            /*		if(g_fFloor == 0) // 向上跳
                        velocity.y += 1000;
                    else*/		
                        this.velocity.y += 350;	//向下跳
                    this.g_fFloor = 0;
                }
                this.canJump = false;
                break;
            
            case 78://n
            break;
        }
    }

    private onKeyUp = (event:any)=>{
        switch ( event.keyCode ) {
            case 38: // up
            case 87: // w
                this.moveForward = false;
                break;
            case 37: // left
            case 65: // a  
                this.moveLeft = false;
                break;
            case 40: // down
            case 83: // s
                this.moveBackward = false;
                break;
            case 39: // right
            case 68: // d
                this.moveRight = false;
                break;
            case 82: // 'R'
            {
                if( this.bCollision == false )
                {
                    this.bCollision = true;
                       $('.myText').html("漫游模式已打开,按R键切换");
                       this.canJump = true;
                   }
                   else
                   {
                        this.bCollision = false;
                       $('.myText').html("飞行模式已打开,,按R键切换");
                   }
            }
            break;					
        }
    }

    private initGUI(){
        let _this = this
        const datGui = new GUI()
        let lightFolder = datGui.addFolder( '控制窗口' );
        lightFolder.add( this.gui, '移动速度',500,2000 ).step( 10 ).onChange( function ( val:number ) {
            _this.m_fSpeed = val;
        } );
        lightFolder.add( this.gui, '视野', 45, 120 ).step( 1 ).onChange( function ( val:number ) {
            _this.camera.fov = val;
            _this.camera.updateProjectionMatrix();
        } );
        lightFolder.add( this.gui, '环境光', 0.3, 3 ).step( 0.01 ).onChange( function ( val:number ) {
            _this.ambientLight.intensity = val;
        } );
        lightFolder.add( this.gui, '镜头光', 50, 500 ).step( 10 ).onChange( function ( val:number ) {
            _this.mLightPos.distance  = val;
        } );	
        //天空
        let folder = datGui.addFolder( '天空' );
        folder.add( this.gui, '太阳倾向',  -0.16, 0.5, 0.0001 ).onChange( function(elevation:number){
            _this.parameters.elevation = elevation;
            _this.updateSun();
        } );
        folder.add( this.gui, '太阳方位角', 0, 1, 0.0001 ).onChange( function(azimuth:number){
            _this.parameters.azimuth = azimuth;
            _this.updateSun();
        } );
    }
    public removeGUI(){
        let GUIDom:HTMLElement|null = document.querySelector('.lil-gui')
        if(GUIDom){
            GUIDom.remove()
        }
    }
    private updateSun(){

        const uniforms = this.sky.material.uniforms;
        uniforms[ 'turbidity' ].value =  this.parameters.turbidity;
        uniforms[ 'rayleigh' ].value =  this.parameters.rayleigh;
        uniforms[ 'mieCoefficient' ].value =  this.parameters.mieCoefficient;
        uniforms[ 'mieDirectionalG' ].value =  this.parameters.mieDirectionalG;

        let theta = Math.PI * ( this.parameters.elevation - 0.85 );
        let phi = 2 * Math.PI * ( this.parameters.azimuth - 0.5 );

        this.derectionLight.position.x = this.parameters.distance * Math.cos( phi );
        this.derectionLight.position.y = this.parameters.distance * Math.sin( phi ) * Math.sin( theta );
        this.derectionLight.position.z = this.parameters.distance * Math.sin( phi ) * Math.cos( theta );

        this.sky.material.uniforms[ 'sunPosition' ].value = this.derectionLight.position.copy( this.derectionLight.position );
    }

    public animate(){
        // TWEEN.update();
	    requestAnimationFrame( () => this.animate() );
        // 如果鼠标被捕获
        if ( this.controls.isLocked === true ) {

            // 向一个方向移动
            this.camera.getWorldDirection( this.direction1 );
            this.mLightPos.position.set(this.camera.position.x,this.camera.position.y,this.camera.position.z );
            this.mLightPos.position.add( this.direction1.multiplyScalar(this.distance) );

            this.raycaster?.ray.origin.copy( this.controls.getObject().position );
            if(this.raycaster){
                this.raycaster.ray.origin.y -= 10
            }

            let intersections = this.raycaster?.intersectObjects( this.objects );

            let onObject = intersections&&intersections.length > 0;	

            let time = performance.now();

            let delta = ( time - this.prevTime ) / this.m_fSpeed;

            
            this.velocity.x -= this.velocity.x * 10.0 * delta;
            this.velocity.z -= this.velocity.z * 10.0 * delta;
            this.velocity.y -= 9.8 * 100.0 * delta; // 100.0 = mass

            this.direction.z = Number( this.moveForward ) - Number( this.moveBackward );
            this.direction.x = Number( this.moveLeft ) 	- Number( this.moveRight );
            this.direction.normalize();	
                                
            if ( onObject == true && this.bCollision ==true) {
                    
                this.controls.getObject().translateX( this.velocity.x * delta );
                this.controls.getObject().position.y += ( this.velocity.y * delta ); // new behavior
                this.controls.getObject().translateZ( this.velocity.z * delta );
            
                this.prevTime = time;
                this.render.render( this.scene, this.camera );
                return;		
            }
            else
            {
                if ( this.moveForward || this.moveBackward ) this.velocity.z -= this.direction.z * 4000.0 * delta;
                if ( this.moveLeft || this.moveRight ) 	   this.velocity.x -= this.direction.x * 4000.0 * delta;	
            }
            
            this.controls.getObject().translateX( this.velocity.x * delta );
            this.controls.getObject().translateZ( this.velocity.z * delta );
            if(this.bCollision == true)
            {
                this.controls.getObject().position.y += ( this.velocity.y * delta ); // new behavior					
            }

            if ( this.controls.getObject().position.y < this.g_fFloor+160 ) {
                this.velocity.y = 0;
                this.controls.getObject().position.y = this.g_fFloor+160;
                this.canJump = true;
            }

            this.prevTime = time;
        }

	    this.render.render( this.scene, this.camera );
    }


    public cleanup(){
        this.clearup()
        //在清除场景的时候，移除事件监听
            //监听事件
            document.removeEventListener( 'keydown', this.onKeyDown, false );
            document.removeEventListener( 'keyup', this.onKeyUp, false );
            // 监听dom元素的点击事件，如果点击了，激活控制器指针锁定
            document.removeEventListener( 'click',this.lockedControls_first, false );
            //当指针状态为 “locked” （即鼠标被捕获）时触发
            this.controls.removeEventListener( 'lock', this.lockedControls_second);
            //当指针状态为 “unlocked” （即鼠标不再被捕获）时触发
            this.controls.removeEventListener( 'unlock', this.unlocked );
        }
     
}
export default Roam 