import GlobalConfig from '../../Utils/Global/threeGlobalConfig';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js'
import { pageStatusStore } from '../../store/pageStatus';
import * as THREE from "three";

//未来处理模型加载状态会用到
const modelStatus = pageStatusStore();


export default class Test extends GlobalConfig {
    public controls: OrbitControls;
    public conatiner:any;
    public sprite:THREE.Sprite;
    constructor() {
        super();
        this.rendererCoord();
        this.configCamera();
        this.configAmbientLight();
        this.configDirectionalLight();
        // modelStatus.modelLoadingStatus = true;
        this.controls = new OrbitControls(this.camera, this.render.domElement);
        this.controls.maxPolarAngle = Math.PI * 0.5;
        this.controls.minDistance = 1;
        this.controls.maxDistance = 2000;
        this.controls.autoRotate = true;
        this.controls.autoRotateSpeed = 0.5;
        this.controls.enablePan = true;
        //更新环境光
        this.ambientLight.intensity = 0.45;
        this.ambientLight.position.set(0, 0, 800);

        this.conatiner = document.getElementById( 'container' );
        this.conatiner?.appendChild(this.render.domElement)

        this.sprite = new THREE.Sprite(new THREE.SpriteMaterial({}))

        this.initObject();
        this.animate();
    }

    public initObject() {
        // this.scene.background = new THREE.Color(0xffffff);
        // 创建一个视频元素
        var video = document.createElement('video');
        video.src = './src/assets/special/火焰爆炸蘑菇云.mp4';
        video.autoplay = true;
        video.loop = true;
        // setTimeout(() => {
        var videoTexture = new THREE.VideoTexture(video);
        videoTexture.minFilter = THREE.LinearFilter;
        videoTexture.magFilter = THREE.LinearFilter;
        videoTexture.format = THREE.RGBAFormat;

        // 创建Sprite材质
        var spriteMaterial = new THREE.SpriteMaterial({map: videoTexture});
        console.log(spriteMaterial)
        spriteMaterial.onBeforeCompile = (shader) => {
            console.log(shader.fragmentShader)
            // 如果是黑色的话就显示为透明
            shader.fragmentShader = shader.fragmentShader.replace(
                // '#include <fog_fragment>','#include <fog_fragment> \n if(gl_FragColor.r>0.0&&gl_FragColor.r<5.0&&gl_FragColor.g>0.0&&gl_FragColor.g<5.0&&gl_FragColor.b>0.0&&gl_FragColor.b<5.0||gl_FragColor == vec4(0.0,0.0,0.0,1.0)){gl_FragColor=vec4(0.0,0.0,0.0,0.0);};'


                // vec4 color = texture2D(texture, vUv);
                // color.r = 1.0; // 将红色通道的值设为1.0
                // gl_FragColor = color;



                '#include <fog_fragment>','#include <fog_fragment> \n if(gl_FragColor == vec4(0.0,0.0,0.0,1.0)){gl_FragColor=vec4(0.0,0.0,0.0,0.0);};'
                )
                
            // shader.fragmentShader = shader.fragmentShader.replace('#include <fog_fragment>','#include <fog_fragment> \n if(gl_FragColor.rgba== vec4(0.0,0.0,0.0,1.0)){gl_FragColor=vec4(0.0,0.0,0.0,0.0)};')
        }

        // 创建Sprite对象
        this.sprite = new THREE.Sprite(spriteMaterial);
        this.sprite.scale.set(30, 30, 1);

        // 将Sprite添加到场景中
        this.scene.add(this.sprite);

        // 创建PlaneGeometry对象
        
        // var videoMaterial = new THREE.MeshBasicMaterial({ map: videoTexture ,side:THREE.DoubleSide});
        // var videoPlane = new THREE.Mesh(new THREE.PlaneGeometry(20, 20), videoMaterial);
        // videoPlane.position.set(0, 0, -1);
        // this.scene.add(videoPlane);
    // }, 1000)


    }

    public animate(){                                        
        requestAnimationFrame(() => this.animate());
        this.render.render(this.scene, this.camera);
        // 切换纹理 ·
        // if(this.sprite.material.map){
            // this.sprite.material.map.offset.x += 0.1
            // console.log('1')
        // }
    }
    public onWindowResize() {
        this.camera.aspect = window.innerWidth / window.innerHeight;
        this.camera.updateProjectionMatrix();
        this.render.setSize(window.innerWidth, window.innerHeight);
    }
    public cleanup(){
        this.clearup()
    }
    public removeGUI(){
        let GUIDom:HTMLElement|null = document.querySelector('.lil-gui')
        if(GUIDom){
            GUIDom.remove()
        }
    }
}