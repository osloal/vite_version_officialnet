import GlobalConfig from '../../Utils/Global/threeGlobalConfig';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js'
import { pageStatusStore } from '../../store/pageStatus';

//未来处理模型加载状态会用到
const modelStatus = pageStatusStore();


export default class Test extends GlobalConfig {
    public controls: OrbitControls;
    public conatiner:any;
    constructor() {
        super();
        this.rendererCoord();
        this.configCamera();
        this.configAmbientLight();
        this.configDirectionalLight();
        // modelStatus.modelLoadingStatus = true;
        this.controls = new OrbitControls(this.camera, this.render.domElement);
        this.controls.maxPolarAngle = Math.PI * 0.5;
        this.controls.minDistance = 1;
        this.controls.maxDistance = 2000;
        this.controls.autoRotate = true;
        this.controls.autoRotateSpeed = 0.5;
        this.controls.enablePan = true;
        //更新环境光
        this.ambientLight.intensity = 0.45;
        this.ambientLight.position.set(0, 0, 800);

        this.conatiner = document.getElementById( 'container' );
        this.conatiner?.appendChild(this.render.domElement)

        this.animate();
    }
    public animate(){                                        
        requestAnimationFrame(() => this.animate());
        this.render.render(this.scene, this.camera);
    }
    public onWindowResize() {
        this.camera.aspect = window.innerWidth / window.innerHeight;
        this.camera.updateProjectionMatrix();
        this.render.setSize(window.innerWidth, window.innerHeight);
    }
    public cleanup(){
        this.clearup()
    }
    public removeGUI(){
        let GUIDom:HTMLElement|null = document.querySelector('.lil-gui')
        if(GUIDom){
            GUIDom.remove()
        }
    }
}