//线性动画
import * as THREE from 'three';
// import TWEEN, { Tween } from 'tween/tween'
// import TWEEN, { Tween } from '@tweenjs/tween.js'
import * as TWEEN from '@tweenjs/tween.js'
import GlobalConfig from '../../Utils/Global/threeGlobalConfig';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js'
import { pageStatusStore } from '../../store/pageStatus';
import { Line2 } from "three/examples/jsm/lines/Line2";
import { LineGeometry } from "three/examples/jsm/lines/LineGeometry";
import { LineMaterial } from "three/examples/jsm/lines/LineMaterial";
import ttt from '@/assets/Roam/groundMap/texture.jpg'

//未来处理模型加载状态会用到
const modelStatus = pageStatusStore();


export default class Test extends GlobalConfig {
    public controls: OrbitControls;
    public conatiner:any;
    constructor() {
        super();
        this.rendererCoord();
        this.configCamera();
        this.configAmbientLight();
        this.configDirectionalLight();
        // modelStatus.modelLoadingStatus = true;
        this.controls = new OrbitControls(this.camera, this.render.domElement);
        this.controls.maxPolarAngle = Math.PI * 0.5;
        this.controls.minDistance = 1;
        this.controls.maxDistance = 2000;
        this.controls.autoRotate = true;
        this.controls.autoRotateSpeed = 0.5;
        this.controls.enablePan = true;
        //更新环境光
        this.ambientLight.intensity = 0.45;
        this.ambientLight.position.set(0, 0, 800);

        this.conatiner = document.getElementById( 'container' );
        this.conatiner?.appendChild(this.render.domElement)

		this.addObject();
		
        this.animate();
    }

    public animate(){                                        
        requestAnimationFrame(() => this.animate());
        TWEEN.update()
        this.render.render(this.scene, this.camera);
    }
    public onWindowResize() {
        this.camera.aspect = window.innerWidth / window.innerHeight;
        this.camera.updateProjectionMatrix();
        this.render.setSize(window.innerWidth, window.innerHeight);
    }
    public cleanup(){
        this.clearup()
    }
    public removeGUI(){
        let GUIDom:HTMLElement|null = document.querySelector('.lil-gui')
        if(GUIDom){
            GUIDom.remove()
        }
    }

	public addObject(){
		// 动画承载屏
		let plane = new THREE.PlaneGeometry(50, 50);
		let planeMaterial = new THREE.MeshBasicMaterial({color: 0xffffff,side:THREE.DoubleSide,opacity: 0.5, transparent: true ,depthTest:false});
		let planeMesh = new  THREE.Mesh(plane, planeMaterial);
		// this.scene.add(planeMesh);

		// 在屏幕上添加一个线条
		let lineGeometry = new LineGeometry();
        let paths: number [] = [];
		paths.push(0, 0, 0);
		paths.push(0, 10, 0);
		paths.push(10, 10, 0);
		paths.push(10, 20, 0);

        lineGeometry.setPositions(paths);

        let colors: number[] = []
        colors.push(1, 1, 0.5);
        lineGeometry.setColors(colors);
        let matLine = new LineMaterial({ depthTest: false, color: 0xffffff, linewidth: 0.005 });
        var line = new Line2(lineGeometry, matLine);
        line.name = "animation_line";
        // this.scene.add(line);

        let sphere: THREE.Mesh[] = [];
        const ballGeo = new THREE.SphereGeometry(1, 32, 32);
        const ballMaterial = new THREE.MeshLambertMaterial({ color: 0xff0000 });
        var _sphere = new THREE.Mesh(ballGeo, ballMaterial);
        _sphere.position.set(0,0,0);
        sphere.push(_sphere);

        // this.scene.add(_sphere);

        // 几何体动画
        var curve = new THREE.CatmullRomCurve3([
            new THREE.Vector3(-20,10,10),
            new THREE.Vector3(-10,15,10),
            new THREE.Vector3(0,10,0),
            new THREE.Vector3(10,15,10),
            new THREE.Vector3(20,10,10),
            new THREE.Vector3(30,15,10),
            new THREE.Vector3(40,15,10)
        ])
        
        // 曲线
        var pathGeometry = new THREE.BufferGeometry().setFromPoints(curve.getPoints(50));
        var pathMaterial = new THREE.LineBasicMaterial({ color: 0xff0000 });
        var path = new THREE.Line(pathGeometry, pathMaterial);
        this.scene.add(path);
        const points = curve.getPoints(50)
        // 创建飞线
        const pointsOnLine = points.slice(0, 10); //从曲线上获取一段
        const flyLineGeom = new THREE.BufferGeometry();
        flyLineGeom.setFromPoints(pointsOnLine);

        // 操作颜色
        const colorArr: number[] = [];
        for (let i = 0; i < pointsOnLine.length; i++) {
            const color1 = new THREE.Color(0X00FFFF); // 线颜色
            const color2 = new THREE.Color(0XFFFF00); // 飞痕颜色
            // 飞痕渐变色
            let color = color1.lerp(color2, i / 5);
            colorArr.push(color.r, color.g, color.b);
        }
        // 设置几何体顶点颜色数据
        flyLineGeom.setAttribute( 'color', new THREE.BufferAttribute( new Float32Array(colorArr), 3 ));
        flyLineGeom.attributes.position.needsUpdate = true;

        const material = new THREE.LineBasicMaterial({
            vertexColors: true, //使用顶点本身颜色
        });

        const flyLine =  new THREE.Line(flyLineGeom, material);
        this.scene.add(flyLine);

        let tween = new TWEEN.Tween({ index: 0 })
        .to({ index: 100 }, 5000)
        .onUpdate(function (t:any) {
          let movedLineGeom = flyLine.geometry
          let id = Math.ceil(t.index);
          let pointsList = points.slice(id, id + 10); //从曲线上获取一段
          movedLineGeom && movedLineGeom.setFromPoints(pointsList);
          movedLineGeom.attributes.position.needsUpdate = true;
        })
        .repeat(Infinity);
        tween.start();


        // 纹理材质动画
        const lineCurve = new THREE.CatmullRomCurve3([
            new THREE.Vector3(-20,0,0),
            new THREE.Vector3(-10,5,0),
            new THREE.Vector3(0,0,0),
            new THREE.Vector3(10,5,0),
            new THREE.Vector3(20,0,0),
            new THREE.Vector3(30,5,0),
            new THREE.Vector3(40,5,0)
        ])

        const curveGeo = new THREE.TubeGeometry(lineCurve,100,1,8,false);

        const texture = new THREE.TextureLoader().load(ttt)
        texture.repeat.set(10,2);
        texture.needsUpdate = true;
        texture.wrapS = THREE.RepeatWrapping
        texture.wrapT = THREE.RepeatWrapping

        const curveMat = new THREE.MeshBasicMaterial({
            map:texture,
            transparent:true,
        })
        const ccc = new THREE.Mesh(curveGeo,curveMat)
        this.scene.add(ccc)

        let  tweent = new TWEEN.Tween({index:0}).to({index:1000},3000)
            .onUpdate(()=>{
                texture.offset.x -= 0.001
            })
            .repeat(Infinity)
            .start()


        // 着色器动画
        const pointInLine = [
            new THREE.Vector3(-5,0,30),
            new THREE.Vector3(0,5,30),
            new THREE.Vector3(30,5,20)
        ];
        
        
        const lineCurve2 = new THREE.CatmullRomCurve3(pointInLine);
        
        const points2 = lineCurve2.getPoints(1000);
        
        const indexList: number[] = [];
        const positionList: number[] = [];
        points2.forEach((item, index) => {
            indexList.push(index)
        })
        
        const geometry = new THREE.BufferGeometry().setFromPoints(points2);
        geometry.setAttribute('aIndex', new THREE.Float32BufferAttribute(indexList, 1))
        
        const starMaterial = new THREE.ShaderMaterial({
            uniforms: {
                uColor: {
                    value: new THREE.Color(0XFFFF00)
                },
                uTime: {
                    value: 0,
                },
                uLength: {
                    value: points2.length,
                },
            },
            vertexShader: `
                attribute float aIndex;
            
                uniform float uTime;
                uniform vec3 uColor;
            
                varying float vSize;
            
                void main(){
                    vec4 viewPosition = viewMatrix * modelMatrix *vec4(position,1);
                    gl_Position = projectionMatrix * viewPosition;
            
                    if(aIndex < uTime + 100.0 && aIndex > uTime - 100.0){
                    vSize = (aIndex + 100.0 - uTime) / 60.0;
                    } 
                    gl_PointSize =vSize;
                }`,
            fragmentShader: `
                varying float vSize;
                uniform vec3 uColor;
                void main(){

                    if(vSize<=0.0){
                        gl_FragColor = vec4(1,0,0,0);
                    }else{
                        gl_FragColor = vec4(uColor,1);
                    }
                    
                }`,
            // vertexShader: `
            //     attribute float aIndex;
            //     uniform float uTime;
            //     uniform float uLength;
            //     uniform float uScale;
            //     varying float vSize;
            //     void main() {
            //         vec4 modelPosition = modelMatrix * vec4( position, 1.0 ); // 顶点坐标
            //         gl_Position =  projectionMatrix * viewMatrix * modelPosition;
            //         float size = aIndex  - uTime; 
            //         if(size < 0.0) {
            //         size = size + uLength;
            //         }
            //         vSize = (size - (uLength * 0.99)); // 只显示0.01的点
            //         gl_PointSize = vSize<=2.0?2.0:vSize;
            //     }
            // `,
            // fragmentShader: `
            //     varying float vSize;
            //     uniform vec3 uColor;
            //     void main(){
            //         float distanceToCenter = distance(gl_PointCoord,vec2(0.5,0.5));
            //         float alpha = 1.0 - (distanceToCenter*2.0);
            //         if(vSize<=2.0){
            //         gl_FragColor = vec4(1,0,1,alpha);
            //         }else{
            //         gl_FragColor = vec4(uColor,alpha);
            //         }
            //     }
            // `,
            transparent: true,
        })
        
        this.scene.add(new THREE.Points(geometry, starMaterial));
        
        let starTween = new TWEEN.Tween({index:0}).to({index:1000},2000)
        starTween.onUpdate((t:any)=>{
            let id = Math.ceil(t.index);
            starMaterial.uniforms.uTime.value = id
        })
        starTween.repeat(Infinity)
        starTween.start();


        // const PlaneGeometry = new THREE.PlaneGeometry(10,10)
        // const PlaneMaterial = new THREE.RawShaderMaterial({
        //     vertexShader:`
        //         uniform mat4 projectionMatrix;
        //         uniform mat4 viewMatrix;
        //         uniform mat4 modelMatrix;

        //         attribute vec3 position;
                
        //         void main(){
        //             vec4 modelPosition = modelMatrix*vec4(position,1.0);
        //             vec4 viewPosition = viewMatrix*modelPosition;
                  
        //             modelPosition.z += sin(modelPosition.x*10.0)*0.1;
        //             gl_Position = projectionMatrix*viewMatrix*modelMatrix*vec4(position,1.0);
        //             gl_Position.x += 0.5;
        //         }
        //     `,
        //     fragmentShader:`
        //         precision mediump float;

        //         void main(){
        //             gl_FragColor = vec4(1.0,.0,0.0,1.0);
        //         }
        //     `,
        //     // wireframe:true,   //网格
        // })
        // const planeRed = new THREE.Mesh(PlaneGeometry,PlaneMaterial)
        // planeRed.position.set(0,20,-10)
        // this.scene.add(planeRed)
        // var _sphereCloned = _sphere.clone()
        // this.scene.add(_sphereCloned)
        // let _spheres = [] as any
        // for(let i = 0;i<3;i++){
        //     var _sphereCloned = _sphere.clone()
        //     this.scene.add(_sphereCloned)
        //     _spheres.push(_sphereCloned)
        // }
        // console.log("球",_spheres)

        // // for(let i = 0;i<_spheres.length;i++){
        //     var position = { t: 0 };
        //     var target = { t: 1 };
        //     var tween = new TWEEN.Tween(position).to(target, 1000)
        //         // .yoyo(true) 
        //         .repeat(Infinity)
        //         // .easing(TWEEN.Easing.Elastic.InOut)
        //         .onUpdate((obj:any)=>{
        //             var pointOnCurve = curve.getPointAt(obj.t);
        //             _spheres[0].position.copy(pointOnCurve);
        //         })
        //         .start();
        // // }
        // var tween5 = new TWEEN.Tween(position).to(target, 1200)
        // // .yoyo(true) 
        // .repeat(Infinity)
        // // .easing(TWEEN.Easing.Elastic.InOut)
        // .onUpdate((obj:any)=>{
        //     var pointOnCurve = curve.getPointAt(obj.t);
        //     _spheres[1].position.copy(pointOnCurve);
        // })
        // .start();
        // var tween4 = new TWEEN.Tween(position).to(target, 1400)
        // // .yoyo(true) 
        // .repeat(Infinity)
        // // .easing(TWEEN.Easing.Elastic.InOut)
        // .onUpdate((obj:any)=>{
        //     var pointOnCurve = curve.getPointAt(obj.t);
        //     _spheres[2].position.copy(pointOnCurve);
        // })
        // .start();
        
        // const tween1 = new TWEEN.Tween(_sphere.position).to({x:0,y:10},2000)
        // const tween2 = new TWEEN.Tween(_sphere.position).to({x:10,y:10},2000)
        // const tween3 = new TWEEN.Tween(_sphere.position).to({x:10,y:20},2000)
        // tween2.chain(tween3)
        // tween1.chain(tween2)
        // tween3.chain(tween1)
        // tween1.start()
	}
}