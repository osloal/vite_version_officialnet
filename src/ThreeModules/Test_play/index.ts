//会展
import * as THREE from 'three';
import GlobalConfig from '../../Utils/Global/threeGlobalConfig';
import { pageStatusStore } from '../../store/pageStatus';

import {GLTFLoader} from 'three/examples/jsm/loaders/GLTFLoader.js';

import { Octree } from 'three/examples/jsm/math/Octree.js';
// import { ViewHelper } from '../../lib/ViewHelper';
// import { OctreeHelper } from 'src/math/OctreeHelper.js';

import { Capsule } from 'three/examples/jsm/math/Capsule.js';

import { GUI } from 'lil-gui';
//未来处理模型加载状态会用到
const modelStatus = pageStatusStore();

const clock = new THREE.Clock();

const GRAVITY = 30;
const NUM_SPHERES = 100;
const SPHERE_RADIUS = 0.2;

const STEPS_PER_FRAME = 5;

const sphereGeometry = new THREE.IcosahedronGeometry( SPHERE_RADIUS, 5 );
const sphereMaterial = new THREE.MeshLambertMaterial( { color: 0xbbbb44 } );

interface ISphereState {mesh:THREE.Mesh, collider:THREE.Sphere, velocity:THREE.Vector3}
const spheres:ISphereState[] = [];


let sphereIdx = 0;



const worldOctree = new Octree();

const playerCollider = new Capsule( new THREE.Vector3( 0, 0.35, 0 ), new THREE.Vector3( 0, 1, 0 ), 0.35 );

const playerVelocity = new THREE.Vector3();
const playerDirection = new THREE.Vector3();

let playerOnFloor = false;
let mouseTime = 0;

interface IKeyState {[key: string]: boolean}
const keyStates: IKeyState = {}

const vector1 = new THREE.Vector3();
const vector2 = new THREE.Vector3();
const vector3 = new THREE.Vector3();

export default class Test extends GlobalConfig {
    public container:any;

    constructor() {
        super();
        // this.rendererCoord();
        this.configCamera();
        this.configAmbientLight();
        this.configDirectionalLight();

        this.scene.background = new THREE.Color( 0x88ccee );
        this.scene.fog = new THREE.Fog( 0x88ccee, 0, 50 );

        this.camera = new THREE.PerspectiveCamera( 70, window.innerWidth / window.innerHeight, 0.1, 1000 );
        this.camera.rotation.order = 'YXZ';

        // 半球光：										天空颜色	地面颜色	光强度
        const fillLight1 = new THREE.HemisphereLight( 0x4488bb, 0x002244, 0.5 );
        // 光线从上往下照射
        fillLight1.position.set( 0, 1, 0 );
        this.scene.add( fillLight1 );

        this.derectionLight.position.set( - 5, 25, - 1 );
        this.derectionLight.castShadow = true;
        this.derectionLight.shadow.camera.near = 0.01;
        this.derectionLight.shadow.camera.far = 500;
        this.derectionLight.shadow.camera.right = 30;
        this.derectionLight.shadow.camera.left = - 30;
        this.derectionLight.shadow.camera.top	= 30;
        this.derectionLight.shadow.camera.bottom = - 30;
        this.derectionLight.shadow.mapSize.width = 1024;
        this.derectionLight.shadow.mapSize.height = 1024;
        this.derectionLight.shadow.radius = 4;
        this.derectionLight.shadow.bias = - 0.00006;
        this.scene.add( this.derectionLight );
        //更新环境光
        this.ambientLight.intensity = 0.45;
        this.ambientLight.position.set(0, 0, 800);

        this.container = document.getElementById( 'container' );
        // this.conatiner?.appendChild(this.render.domElement)

			this.render.setPixelRatio( window.devicePixelRatio );
			this.render.setSize( window.innerWidth, window.innerHeight );
			this.render.shadowMap.enabled = true;
			this.render.shadowMap.type = THREE.VSMShadowMap;
			this.render.outputEncoding = THREE.sRGBEncoding;
			this.render.toneMapping = THREE.ACESFilmicToneMapping;
			this.container.appendChild( this.render.domElement );

			// const stats = new Stats();
			// stats.domElement.style.position = 'absolute';
			// stats.domElement.style.top = '0px';
			// this.container.appendChild( stats.domElement );

            for ( let i = 0; i < NUM_SPHERES; i ++ ) {

				const sphere = new THREE.Mesh( sphereGeometry, sphereMaterial );
				sphere.castShadow = true;
				sphere.receiveShadow = true;

				this.scene.add( sphere );

				spheres.push( {
					mesh: sphere,
					collider: new THREE.Sphere( new THREE.Vector3( 0, - 100, 0 ), SPHERE_RADIUS ),
					velocity: new THREE.Vector3()
				} );

			}
			

            // // 触发事件
            // this.render.domElement.addEventListener('mousedown', this.mouseDown.bind(this), false);
            // this.render.domElement.addEventListener('mousemove', this.mouseMove.bind(this), false);
            // this.render.domElement.addEventListener('mouseup', this.mouseUp.bind(this), false);
            // //监听事件
            // document.addEventListener( 'keydown', this.onKeyDown, false );
            // document.addEventListener( 'keyup', this.onKeyUp, false );
            
            document.addEventListener( 'keydown', ( event ) => {

				keyStates[ event.code ] = true;

			} );

			document.addEventListener( 'keyup', ( event ) => {

				keyStates[ event.code ] = false;

			} );

			this.container.addEventListener( 'mousedown', () => {

				document.body.requestPointerLock();

				mouseTime = performance.now();

			} );

			// mouseup发射球

			// document.addEventListener( 'mouseup', () => {

			// 	if ( document.pointerLockElement !== null ) 
			// 	throwBall();

			// } );

			document.body.addEventListener( 'mousemove', ( event ) => {
          
				if ( document.pointerLockElement === document.body ) {

					this.camera.rotation.y -= event.movementX / 500;
					this.camera.rotation.x -= event.movementY / 500;

				}

			} );


            const loader = new GLTFLoader();

			loader.load( './src/assets/models/huiZhan/FD.glb', ( gltf ) => {
			// loader.load( 'collision-world.glb', ( gltf ) => {

				this.scene.add( gltf.scene );

				worldOctree.fromGraphNode( gltf.scene );

				gltf.scene.traverse( child => {

					// if ( child.isMesh ) {

					// 	child.castShadow = true;
					// 	child.receiveShadow = true;

					// 	if ( child.material.map ) {
					// 		// 各向异性过滤可以根据贴图中的像素密度和相对于采样方向的角度来确定采样率，从而提高模型的视觉质量
					// 		child.material.map.anisotropy = 4;

					// 	}

					// }

				} );

				// const helper = new OctreeHelper( worldOctree );
				// helper.visible = false;
				// scene.add( helper );

				// const gui = new GUI( { width: 200 } );
				// gui.add( { debug: false }, 'debug' )
				// 	.onChange( function ( value ) {

				// 		helper.visible = value;

				// 	} );


			} ,function(xhr){
					console.log((xhr.loaded / xhr.total * 100) + '% loaded');
				},function(error){
					console.log('An error happened');
				});


		this.animate();

    }
    	// 丢球
			// private throwBall() {

			// 	const sphere = spheres[ sphereIdx ];

			// 	camera.getWorldDirection( playerDirection );

			// 	sphere.collider.center.copy( playerCollider.end ).addScaledVector( playerDirection, playerCollider.radius * 1.5 );

			// 	// throw the ball with more force if we hold the button longer, and if we move forward

			// 	const impulse = 15 + 30 * ( 1 - Math.exp( ( mouseTime - performance.now() ) * 0.001 ) );

			// 	sphere.velocity.copy( playerDirection ).multiplyScalar( impulse );
			// 	sphere.velocity.addScaledVector( playerVelocity, 2 );

			// 	sphereIdx = ( sphereIdx + 1 ) % spheres.length;

			// }


			// 玩家碰撞
			private playerCollisions() {

				const result = worldOctree.capsuleIntersect( playerCollider );

				playerOnFloor = false;

				if ( result ) {
					
					playerOnFloor = result.normal.y > 0;

					if ( ! playerOnFloor ) {

						playerVelocity.addScaledVector( result.normal, - result.normal.dot( playerVelocity ) );

					}

					playerCollider.translate( result.normal.multiplyScalar( result.depth ) );

				}

			}

			private updatePlayer( deltaTime:any ) {

				let damping = Math.exp( - 4 * deltaTime ) - 1;

				if ( ! playerOnFloor ) {

					playerVelocity.y -= GRAVITY * deltaTime;

					// small air resistance
					damping *= 0.1;

				}

				playerVelocity.addScaledVector( playerVelocity, damping );

				const deltaPosition = playerVelocity.clone().multiplyScalar( deltaTime );
				playerCollider.translate( deltaPosition );

				this.playerCollisions();

				this.camera.position.copy( playerCollider.end );

			}

			// 玩家和球体碰撞
			private playerSphereCollision( sphere:any ) {

				const center = vector1.addVectors( playerCollider.start, playerCollider.end ).multiplyScalar( 0.5 );

				const sphere_center = sphere.collider.center;

				const r = playerCollider.radius + sphere.collider.radius;
				const r2 = r * r;

				// approximation: player = 3 spheres

				for ( const point of [ playerCollider.start, playerCollider.end, center ] ) {

					const d2 = point.distanceToSquared( sphere_center );

					if ( d2 < r2 ) {

						const normal = vector1.subVectors( point, sphere_center ).normalize();
						const v1 = vector2.copy( normal ).multiplyScalar( normal.dot( playerVelocity ) );
						const v2 = vector3.copy( normal ).multiplyScalar( normal.dot( sphere.velocity ) );

						playerVelocity.add( v2 ).sub( v1 );
						sphere.velocity.add( v1 ).sub( v2 );

						const d = ( r - Math.sqrt( d2 ) ) / 2;
						sphere_center.addScaledVector( normal, - d );

					}

				}

			}

			// 球体碰撞
			private spheresCollisions() {

				for ( let i = 0, length = spheres.length; i < length; i ++ ) {

					const s1 = spheres[ i ];

					for ( let j = i + 1; j < length; j ++ ) {

						const s2 = spheres[ j ];

						const d2 = s1.collider.center.distanceToSquared( s2.collider.center );
						const r = s1.collider.radius + s2.collider.radius;
						const r2 = r * r;

						if ( d2 < r2 ) {

							const normal = vector1.subVectors( s1.collider.center, s2.collider.center ).normalize();
							const v1 = vector2.copy( normal ).multiplyScalar( normal.dot( s1.velocity ) );
							const v2 = vector3.copy( normal ).multiplyScalar( normal.dot( s2.velocity ) );

							s1.velocity.add( v2 ).sub( v1 );
							s2.velocity.add( v1 ).sub( v2 );

							const d = ( r - Math.sqrt( d2 ) ) / 2;

							s1.collider.center.addScaledVector( normal, d );
							s2.collider.center.addScaledVector( normal, - d );

						}

					}

				}

			}

			private updateSpheres( deltaTime:any ) {

				spheres.forEach( sphere => {

					sphere.collider.center.addScaledVector( sphere.velocity, deltaTime );

					const result = worldOctree.sphereIntersect( sphere.collider );

					if ( result ) {

						sphere.velocity.addScaledVector( result.normal, - result.normal.dot( sphere.velocity ) * 1.5 );
						sphere.collider.center.add( result.normal.multiplyScalar( result.depth ) );

					} else {

						sphere.velocity.y -= GRAVITY * deltaTime;

					}

					const damping = Math.exp( - 1.5 * deltaTime ) - 1;
					sphere.velocity.addScaledVector( sphere.velocity, damping );

					this.playerSphereCollision( sphere );

				} );

				this.spheresCollisions();

				for ( const sphere of spheres ) {

					sphere.mesh.position.copy( sphere.collider.center );

				}

			}

			private getForwardVector() {

				this.camera.getWorldDirection( playerDirection );
				playerDirection.y = 0;
				playerDirection.normalize();

				return playerDirection;

			}

			private getSideVector() {

				this.camera.getWorldDirection( playerDirection );
				playerDirection.y = 0;
				playerDirection.normalize();
				playerDirection.cross( this.camera.up );

				return playerDirection;

			}

			public objControls( deltaTime:any ) {

				// gives a bit of air control
				const speedDelta = deltaTime * ( playerOnFloor ? 25 : 8 );

				if ( keyStates[ 'KeyW' ] ) {
					playerVelocity.add( this.getForwardVector().multiplyScalar( speedDelta ) );

				}

				if ( keyStates[ 'KeyS' ] ) {

					playerVelocity.add( this.getForwardVector().multiplyScalar( - speedDelta ) );

				}

				if ( keyStates[ 'KeyA' ] ) {

					playerVelocity.add( this.getSideVector().multiplyScalar( - speedDelta ) );

				}

				if ( keyStates[ 'KeyD' ] ) {

					playerVelocity.add( this.getSideVector().multiplyScalar( speedDelta ) );

				}

				if ( playerOnFloor ) {

					if ( keyStates[ 'Space' ] ) {

						playerVelocity.y = 15;

					}

				}

			}
    public onWindowResize() {
        this.camera.aspect = window.innerWidth / window.innerHeight;
        this.camera.updateProjectionMatrix();
        this.render.setSize(window.innerWidth, window.innerHeight);
    }
    public cleanup(){
        this.clearup()
    }
    public removeGUI(){
        let GUIDom:HTMLElement|null = document.querySelector('.lil-gui')
        if(GUIDom){
            GUIDom.remove()
        }
    }

    public teleportPlayerIfOob() {

        if ( this.camera.position.y <= - 25 ) {

            playerCollider.start.set( 0, 0.35, 0 );
            playerCollider.end.set( 0, 1, 0 );
            playerCollider.radius = 0.35;
            this.camera.position.copy( playerCollider.end );
            this.camera.rotation.set( 0, 0, 0 );

        }

    }


    private animate() {
        requestAnimationFrame(() => this.animate());
        const deltaTime = Math.min( 0.05, clock.getDelta() ) / STEPS_PER_FRAME;

        // we look for collisions in substeps to mitigate the risk of
        // an object traversing another too quickly for detection.

        for ( let i = 0; i < STEPS_PER_FRAME; i ++ ) {

            this.objControls( deltaTime );

            this.updatePlayer( deltaTime );

            this.updateSpheres( deltaTime );

            this.teleportPlayerIfOob();

        }

        this.render.render( this.scene, this.camera );

        // this.stats.update();
    }
}